package com.example.learning_test;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
  @Autowired
  private UserRepository userRepository;

  public User getUser(int id){
    return userRepository.getOne(id);
  }
}
